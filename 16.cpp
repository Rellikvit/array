// 16.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

const int N = 3;
const int Date = 25;
int Array[N][N] = {};

int main()
{
    int num = Date % N;
    int summ = 0;
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N;j++)
        {
            Array[i][j] = i + j;
            std::cout << Array[i][j];
        }
    }
    std::cout << '\n';
    for (int k = 0;k < N;k++)
    {
        summ += Array[num][k];
    }
    std::cout << summ;
}